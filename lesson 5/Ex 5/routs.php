<?php

use Illuminate\Support\Facades\Route;

Route::get('/users/{email}/{name?}', function ($email=null, $name = null) {
    if(filter_var($email, FILTER_VALIDATE_EMAIL)&& $name != null){
        return view('users', compact('email','name'));

    }elseif (filter_var($email, FILTER_VALIDATE_EMAIL)&& $name == null){
        $name ='name is missing';
        return view('users', compact('email', 'name'));
    }
         elseif($name==null) {
         $email='invalid email, enter valid email!';
         $name ='name is missing';
         return view('users', compact('email','name'));
     }
    else {
        $email='invalid email, enter valid email';
        return view('users', compact('email','name'));
    }
    
});

?>

