<?php

class QueryEx4{
    protected $dbc;
    function __construct($dbc){
        $this->dbc = $dbc;

    }
    public function query($query){
        #the following query return true if the query is working well, false if not
        if($result = $this->dbc->query($query)){
            return $result;
        }else{
            echo ("Something went wrong with the query");
        }
    } 
}


?>